package io.gitlab.arrem.clam

import io.gitlab.arrem.clam.analysis.SemanticChecker
import io.gitlab.arrem.clam.borealis.Generator
import io.gitlab.arrem.clam.lexer.Lexer
import io.gitlab.arrem.clam.parser.Parser
import org.junit.Test
import java.nio.file.Files
import java.nio.file.Paths

class GeneratorTest {
    @Test
    fun arithmetic() {
        testFile("examples/arithmetic.clam")
    }

    @Test
    fun blocks() {
        testFile("examples/blocks.clam")
    }

    @Test
    fun booleans() {
        testFile("examples/booleans.clam")
    }

    @Test
    fun `if`() {
        testFile("examples/if.clam")
    }

    @Test
    fun pi() {
        testFile("examples/pi.clam")
    }

    @Test
    fun `while`() {
        testFile("examples/while.clam")
    }

    @Test
    fun define_functions() {
        testFile("examples/define_functions.clam")
    }

    private fun testFile(path: String) {
        val lines = Files.readAllLines(Paths.get(path)).joinToString("\n")

        val parser = Parser(Lexer(lines))
        val checker = SemanticChecker()
        val generator = Generator(checker.declarations)

        val program = parser.program()

        program.visit(checker)
        program.visit(generator)
    }
}