package io.gitlab.arrem.clam

import io.gitlab.arrem.clam.lexer.*
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class LexerTest {
    @Test
    fun `basic float parsing`() {
        val lexer = Lexer("135 6")

        val token1 = lexer.next()
        assertTrue(token1 is FloatLiteral)
        assertEquals(135f, (token1 as FloatLiteral).value)

        val token2 = lexer.next()
        assertTrue(token2 is FloatLiteral)
        assertEquals(6f, (token2 as FloatLiteral).value)

        assertTrue(lexer.next() is EOFToken)
    }

    @Test
    fun `EOF on input end`() {
        val lexer = Lexer("hello")

        lexer.next()

        assertTrue(lexer.next() is EOFToken)
        assertTrue(lexer.next() is EOFToken)
    }

    @Test
    fun `float parsing with decimals`() {
        val lexer = Lexer("13.625")

        val token = lexer.next()
        assertTrue(token is FloatLiteral)
        assertEquals(13.625f, (token as FloatLiteral).value)
    }

    @Test
    fun `parse operators and symbols`() {
        val lexer = Lexer("+ - * / == <= >= < > ( ) { } = += -= *= /= ; : ,")

        assertTrue(lexer.next() is PlusToken)
        assertTrue(lexer.next() is MinusToken)
        assertTrue(lexer.next() is TimesToken)
        assertTrue(lexer.next() is DivideToken)
        assertTrue(lexer.next() is EqualsToken)
        assertTrue(lexer.next() is LessThanOrEqualToken)
        assertTrue(lexer.next() is GreaterThanOrEqualToken)
        assertTrue(lexer.next() is LessThanToken)
        assertTrue(lexer.next() is GreaterThanToken)
        assertTrue(lexer.next() is LParenToken)
        assertTrue(lexer.next() is RParenToken)
        assertTrue(lexer.next() is LBraceToken)
        assertTrue(lexer.next() is RBraceToken)
        assertTrue(lexer.next() is AssignToken)
        assertTrue(lexer.next() is PlusAssignToken)
        assertTrue(lexer.next() is MinusAssignToken)
        assertTrue(lexer.next() is TimesAssignToken)
        assertTrue(lexer.next() is DivideAssignToken)
        assertTrue(lexer.next() is SemicolonToken)
        assertTrue(lexer.next() is ColonToken)
        assertTrue(lexer.next() is CommaToken)

        assertTrue(lexer.next() is EOFToken)
    }

    @Test
    fun `operator string representation`() {
        val input = "+ - * / == <= >= < > = += -= *= /="
        val lexer = Lexer(input)

        val list = mutableListOf<String>()

        var token: Token = lexer.next()

        while (token !is EOFToken) {
            list.add(token.toString())

            token = lexer.next()
        }

        val joined = list.joinToString(" ")

        assertEquals(input, joined)
    }

    @Test
    fun `ignore comments`() {
        val lexer = Lexer("// Steamed clams.")

        assertTrue(lexer.next() is EOFToken)
    }

    @Test
    fun `unexpected characters`() {
        val lexer = Lexer("$ 25")

        assertFailsWith(LexerException::class, { lexer.next() })
    }

    @Test
    fun `parse keywords`() {
        val lexer = Lexer("let if else while true false")

        assertTrue(lexer.next() is LetToken)
        assertTrue(lexer.next() is IfToken)
        assertTrue(lexer.next() is ElseToken)
        assertTrue(lexer.next() is WhileToken)
        assertTrue(lexer.next() is TrueToken)
        assertTrue(lexer.next() is FalseToken)
        assertTrue(lexer.next() is EOFToken)
    }

    @Test
    fun `parse identifiers`() {
        val lexer = Lexer("hello3let")

        val token1 = lexer.next()
        assertTrue(token1 is IdentifierToken)
        assertEquals("hello", (token1 as IdentifierToken).value)
    }
}
