package io.gitlab.arrem.clam

import io.gitlab.arrem.clam.analysis.SemanticChecker
import io.gitlab.arrem.clam.analysis.SemanticsException
import io.gitlab.arrem.clam.analysis.TypeException
import io.gitlab.arrem.clam.lexer.Lexer
import io.gitlab.arrem.clam.parser.Parser
import org.junit.Test
import kotlin.test.assertFailsWith

class SemanticsTest {
    @Test
    fun `double declaration fails`() {
        val parser = Parser(Lexer("let a = 5; let a = 5;"))
        val checker = SemanticChecker()

        assertFailsWith(
                SemanticsException::class,
                { parser.program().visit(checker) }
        )
    }

    @Test
    fun `undeclared variable assignment fails`() {
        val parser = Parser(Lexer("a = 7.25;"))
        val checker = SemanticChecker()

        assertFailsWith(
                SemanticsException::class,
                { parser.program().visit(checker) }
        )
    }

    @Test
    fun `undeclared identifier fails`() {
        val parser = Parser(Lexer("let a = 2 * b;"))
        val checker = SemanticChecker()

        assertFailsWith(
                SemanticsException::class,
                { parser.program().visit(checker) }
        )
    }

    @Test
    fun `incompatible types fail left`() {
        val parser = Parser(Lexer("let a = 5 > 3; let b = a > 5;"))
        val checker = SemanticChecker()

        assertFailsWith(
                TypeException::class,
                { parser.program().visit(checker) }
        )
    }

    @Test
    fun `incompatible types fail right`() {
        val parser = Parser(Lexer("let a = true; let b = 5 < a;"))
        val checker = SemanticChecker()

        assertFailsWith(
                TypeException::class,
                { parser.program().visit(checker) }
        )
    }

    @Test
    fun `unary operation on booleans fail`() {
        val parser = Parser(Lexer("let a = 5 > 3; let b = -a;"))
        val checker = SemanticChecker()

        assertFailsWith(
                TypeException::class,
                { parser.program().visit(checker) }
        )
    }

    @Test
    fun `type inference succeeds`() {
        val parser = Parser(Lexer("let a = 3 * 5 + 2; let b = a < 3; let c = b; let d = -a + 3;"))
        val checker = SemanticChecker()

        parser.program().visit(checker)
    }

    @Test
    fun `type inference for boolean literal`() {
        val parser = Parser(Lexer("let a = true; a = false;"))
        val checker = SemanticChecker()

        parser.program().visit(checker)
    }

    @Test
    fun `reassignment succeeds`() {
        val parser = Parser(Lexer("let a = 5; a = 7 - a;"))
        val checker = SemanticChecker()

        parser.program().visit(checker)
    }

    @Test
    fun `reassignment cannot change type`() {
        val parser = Parser(Lexer("let a = 5; a = 7 > 3;"))
        val checker = SemanticChecker()

        assertFailsWith(
                TypeException::class,
                { parser.program().visit(checker) }
        )
    }

    @Test
    fun `same name in nested scope`() {
        val parser = Parser(Lexer("let a = 5; { let a = 3; } let b = a + 2;"))
        val checker = SemanticChecker()

        parser.program().visit(checker)
    }

    @Test
    fun `if analysis`() {
        val parser = Parser(Lexer("let a = 3; if true { a = a + 1; }"))
        val checker = SemanticChecker()

        parser.program().visit(checker)
    }


    @Test
    fun `if else analysis`() {
        val parser = Parser(Lexer("let a = 3; if false { a = a + 1; } else { a = 5; }"))
        val checker = SemanticChecker()

        parser.program().visit(checker)
    }

    @Test
    fun `if condition must be bool`() {
        val parser = Parser(Lexer("if 3 + 2 { }"))
        val checker = SemanticChecker()

        assertFailsWith(
                TypeException::class,
                { parser.program().visit(checker) }
        )
    }

    @Test
    fun `while analysis`() {
        val parser = Parser(Lexer("let a = 5 > 3; while a { a = false; }"))
        val checker = SemanticChecker()

        parser.program().visit(checker)
    }

    @Test
    fun `while condition must be bool`() {
        val parser = Parser(Lexer("while 42 { }"))
        val checker = SemanticChecker()

        assertFailsWith(
                TypeException::class,
                { parser.program().visit(checker) }
        )
    }

    @Test
    fun `undefined function`() {
        val parser = Parser(Lexer("foo();"))
        val checker = SemanticChecker()

        assertFailsWith(
                SemanticsException::class,
                { parser.program().visit(checker) }
        )
    }

    @Test
    fun `function arity mismatch`() {
        val parser = Parser(Lexer("println();"))
        val checker = SemanticChecker()

        assertFailsWith(
                SemanticsException::class,
                { parser.program().visit(checker) }
        )
    }

    @Test
    fun `argument type mismatch`() {
        val parser = Parser(Lexer("println(true);"))
        val checker = SemanticChecker()

        assertFailsWith(
                SemanticsException::class,
                { parser.program().visit(checker) }
        )
    }
}