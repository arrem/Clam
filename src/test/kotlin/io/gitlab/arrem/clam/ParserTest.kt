package io.gitlab.arrem.clam

import io.gitlab.arrem.clam.lexer.*
import io.gitlab.arrem.clam.parser.*
import org.junit.Test
import kotlin.reflect.full.memberFunctions
import kotlin.reflect.jvm.isAccessible
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import kotlin.test.fail

class ParserTest {
    @Test
    fun `basic arithmetic expressions`() {
        val parser = Parser(Lexer("3 + 2"))

        Parser::class.memberFunctions.find { it.name == "expression" }?.let {
            it.isAccessible = true
            val result = it.call(parser)

            assert(result is BinaryOperation)

            val op = result as BinaryOperation

            assertTrue(op.left is FloatSymbol)
            assertEquals(3f, (op.left as FloatSymbol).value)

            assertTrue(op.right is FloatSymbol)
            assertEquals(2f, (op.right as FloatSymbol).value)

            assertTrue(op.op is PlusToken)
        } ?: fail("expression method not found.")
    }

    @Test
    fun `parenthesized expressions`() {
        val parser = Parser(Lexer("((2.625) * ((3 + 5.5)))"))

        Parser::class.memberFunctions.find { it.name == "expression" }?.let {
            it.isAccessible = true
            val result = it.call(parser)

            assert(result is BinaryOperation)

            val op = result as BinaryOperation

            assertTrue(op.left is FloatSymbol)
            assertEquals(2.625f, (op.left as FloatSymbol).value)

            assertTrue(op.right is BinaryOperation)
            assertTrue(op.op is TimesToken)

            val op2 = op.right as BinaryOperation

            assertTrue(op2.left is FloatSymbol)
            assertEquals(3f, (op2.left as FloatSymbol).value)

            assertTrue(op2.right is FloatSymbol)
            assertEquals(5.5f, (op2.right as FloatSymbol).value)

            assertTrue(op2.op is PlusToken)
        } ?: fail("expression method not found.")
    }

    @Test
    fun `expressions with unary operators`() {
        val parser = Parser(Lexer("-a + +2"))

        Parser::class.memberFunctions.find { it.name == "expression" }?.let {
            it.isAccessible = true
            val result = it.call(parser)

            assert(result is BinaryOperation)

            val op = result as BinaryOperation

            assertTrue(op.left is UnaryOperation)

            val op2 = op.left as UnaryOperation

            assertTrue(op2.op is MinusToken)

            assertTrue(op2.right is IdentifierExpression)
            assertEquals("a", (op2.right as IdentifierExpression).value)

            assertTrue(op.op is PlusToken)

            assertTrue(op.right is UnaryOperation)

            val op3 = op.right as UnaryOperation

            assertTrue(op3.op is PlusToken)

            assertTrue(op3.right is FloatSymbol)
            assertEquals(2f, (op3.right as FloatSymbol).value)




        } ?: fail("expression method not found.")
    }

    @Test
    fun `identifiers in expressions`() {
        val parser = Parser(Lexer("5.5 / foo"))

        Parser::class.memberFunctions.find { it.name == "expr2" }?.let {
            it.isAccessible = true
            val result = it.call(parser)

            assert(result is BinaryOperation)

            val op = result as BinaryOperation

            assertTrue(op.left is FloatSymbol)
            assertTrue((op.left as FloatSymbol).value == 5.5f)

            assertTrue(op.right is IdentifierExpression)
            assertTrue((op.right as IdentifierExpression).value == "foo")

            assertTrue(op.op is DivideToken)
        } ?: fail("expr2 method not found.")
    }

    @Test
    fun `empty program`() {
        val parser = Parser(Lexer(""))

        val program = parser.program()

        assertEquals(0, program.statements.size)
    }

    @Test
    fun `basic assignment program`() {
        val parser = Parser(Lexer("""
            let a = 5;
            let b = a + 3;
            a = 4;
        """))

        val program = parser.program()

        assertEquals(3, program.statements.size)
        assertTrue(program.statements[0] is Initialization)
        assertTrue(program.statements[1] is Initialization)
        assertTrue(program.statements[2] is Assignment)
    }

    @Test
    fun `expr statement`() {
        val parser = Parser(Lexer("""
            3;
        """))

        val program = parser.program()

        assertEquals(1, program.statements.size)
        assertTrue(program.statements[0] is Expression)
    }

    @Test
    fun `boolean literals`() {
        val parser = Parser(Lexer("let a = true; a = false;"))

        parser.program()
    }

    @Test
    fun `compound assignment`() {
        val parser = Parser(Lexer("""
            let a = 5;
            a += 1;
            a -= 2;
            a *= a;
            a /= 0;
        """))

        parser.program()
    }

    @Test
    fun `basic if statement`() {
        val parser = Parser(Lexer("if false { a = 2; }"))

        parser.program()
    }

    @Test
    fun `if else statement`() {
        val parser = Parser(Lexer("if false { a = 2; } else { a = 3; }"))

        parser.program()
    }

    @Test
    fun `if with assignment`() {
        val parser = Parser(Lexer("if true a = 2;"))

        parser.program()
    }

    @Test
    fun `nested if else`() {
        val parser = Parser(Lexer("if true if false a = 2; else a = 3; else a = 4;"))

        parser.program()
    }

    @Test
    fun `if without body fails`() {
        val parser = Parser(Lexer("if true"))

        assertFailsWith(
                ParserException::class,
                { parser.program() }
        )
    }

    @Test
    fun `dangling else fails`() {
        val parser = Parser(Lexer("if false { a = 2; } else"))

        assertFailsWith(
                ParserException::class,
                { parser.program() }
        )
    }

    @Test
    fun `function calls`() {
        val parser = Parser(Lexer("""
            foo();
            bar(1);
            baz(1, true, 3 > 5);
            foo(bar(baz(), baz(3)));
        """))

        parser.program().statements.forEach {
            assertTrue(it is FunctionCall)
        }
    }

    @Test
    fun `invalid statement`() {
        val parser = Parser(Lexer("3 + 2"))

        assertFailsWith(
                ParserException::class,
                { parser.program() }
        )

    }

    @Test
    fun `unexpected EOF`() {
        val parser = Parser(Lexer("let"))

        assertFailsWith(
                ParserException::class,
                { parser.program() }
        )

    }

    @Test
    fun `unexpected token`() {
        val parser = Parser(Lexer("let let"))

        assertFailsWith(
                ParserException::class,
                { parser.program() }
        )

    }

    @Test
    fun `function declaration`() {
        val parser = Parser(Lexer("fn print() { return; }"))

        parser.program();
    }
}