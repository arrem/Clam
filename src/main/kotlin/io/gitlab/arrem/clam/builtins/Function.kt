package io.gitlab.arrem.clam.builtins

import io.gitlab.arrem.clam.analysis.Type
import io.gitlab.arrem.clam.parser.Argument

abstract class Function(val name: String, val args: List<Argument>, val returnType: Type)

abstract class BuiltinFunction(name: String, args: List<Type>, returnType: Type) :
        Function(name, args.map({ Argument("", it) }), returnType) {
    abstract fun call(args: List<Any>): Any

}