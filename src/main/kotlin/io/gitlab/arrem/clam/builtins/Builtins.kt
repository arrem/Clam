package io.gitlab.arrem.clam.builtins

import io.gitlab.arrem.clam.analysis.Type

val builtins = mutableMapOf(
        "println" to object : BuiltinFunction("println", listOf(Type.Float), Type.Unit) {
            override fun call(args: List<Any>) {
                println(args[0])
            }
        },
        "random" to object : BuiltinFunction("random", listOf(), Type.Float) {
            override fun call(args: List<Any>): Float {
                return Math.random().toFloat()
            }

        }
)