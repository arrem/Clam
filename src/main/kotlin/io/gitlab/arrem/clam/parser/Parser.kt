package io.gitlab.arrem.clam.parser

import io.gitlab.arrem.clam.analysis.Type
import io.gitlab.arrem.clam.lexer.*

/**
 * A ParserException is thrown when an invalid or malformed expression is encountered while parsing the source code.
 *
 * @param message The error message to give. Must always describe the cause precisely.
 */
class ParserException(message: String) : RuntimeException(message)

class Parser(private val lexer: Lexer) {
    private var currentToken = lexer.next()

    fun program(): Program {
        val statements = mutableListOf<ProgramPart>()

        while (currentToken !is EOFToken) {
            statements.add(programPart())
        }

        return Program(statements)
    }

    private fun programPart(): ProgramPart {
        if (currentToken is FnToken) {
            return functionDeclaration()
        }

        return statement()
    }

    private fun functionDeclaration(): FunctionDeclaration {
        consume<FnToken>()

        val name = consume<IdentifierToken>()

        consume<LParenToken>()

        val arguments = mutableListOf<Argument>()

        while (currentToken !is RParenToken) {
            arguments.add(argument())

            if (currentToken is CommaToken) {
                consume<CommaToken>()
            } else {
                break
            }
        }

        consume<RParenToken>()

        val returnType = if (currentToken is ColonToken) {
            consume<ColonToken>()

            consume<TypeDeclaration>().type
        } else {
            Type.Unit
        }

        val body = block()

        return FunctionDeclaration(name.value, arguments, returnType, body)
    }

    private fun argument(): Argument {
        val name = consume<IdentifierToken>()

        consume<ColonToken>()

        val type = consume<TypeDeclaration>()

        return Argument(name.value, type.type)
    }

    private fun block(): Block {
        val statements = mutableListOf<Statement>()

        consume<LBraceToken>()

        while (currentToken !is RBraceToken) {
            statements.add(statement())
        }

        consume<RBraceToken>()

        return Block(statements)
    }

    private fun statement(): Statement {
        if (currentToken is LBraceToken) {
            return block()
        }

        if (currentToken is IfToken) {
            return ifStatement()
        }

        if (currentToken is WhileToken) {
            return whileStatement()
        }

        if (currentToken is LetToken) {
            return initialization()
        }

        if (currentToken is IdentifierToken) {
            val identifier = consume<IdentifierToken>()

            return if (currentToken is AssignmentOperator) {
                assignment(identifier)
            } else {
                val function = functionCall(identifier)
                consume<SemicolonToken>()
                function
            }
        }

        if (currentToken is ReturnToken) {
            consume<ReturnToken>()

            if (currentToken is SemicolonToken) {
                consume<SemicolonToken>()
                return ReturnStatement(null)
            }

            val expr  = expression()

            consume<SemicolonToken>()

            return ReturnStatement(expr)
        }

        val expression = expression()

        consume<SemicolonToken>()

        return expression
    }

    private fun ifStatement(): IfStatement {
        consume<IfToken>()

        val condition = expression()
        val statement = statement()

        if (currentToken !is ElseToken) {
            return IfStatement(condition, statement)
        }

        consume<ElseToken>()

        return IfStatement(condition, statement, statement())
    }

    private fun whileStatement(): WhileStatement {
        consume<WhileToken>()

        val condition = expression()
        val statement = statement()

        return WhileStatement(condition, statement)
    }

    private fun initialization(): Initialization {
        consume<LetToken>()

        val identifier = consume<IdentifierToken>()

        consume<AssignToken>()

        val expr = expression()

        consume<SemicolonToken>()

        return Initialization(identifier, expr)
    }

    private fun assignment(identifier: IdentifierToken): Assignment {
        val op = consume<AssignmentOperator>()

        val expr = expression()

        consume<SemicolonToken>()

        return Assignment(identifier, op, expr)
    }

    private fun functionCall(identifier: IdentifierToken): FunctionCall {
        consume<LParenToken>()

        val args = mutableListOf<Expression>()

        while (currentToken !is RParenToken) {
            args.add(expression())

            if (currentToken is CommaToken) {
                consume<CommaToken>()
            }
        }

        consume<RParenToken>()

        return FunctionCall(identifier.value, args)
    }

    private fun expr0(): Expression {
        if (currentToken is LParenToken) {
            consume<LParenToken>()

            val expr = expression()

            consume<RParenToken>()

            return expr
        }

        if (currentToken is PlusToken) {
            return UnaryOperation(consume(), expr0())
        }

        if (currentToken is MinusToken) {
            return UnaryOperation(consume(), expr0())
        }

        if (currentToken is IdentifierToken) {
            val identifier = consume<IdentifierToken>()

            if (currentToken is LParenToken) {
                return functionCall(identifier)
            }

            return IdentifierExpression(identifier.value)
        }

        if (currentToken is BooleanLiteral) {
            return BooleanSymbol(consume<BooleanLiteral>().value)
        }

        return FloatSymbol(consume<FloatLiteral>().value)
    }


    private fun expr1(): Expression {
        var result = expr0()

        while (currentToken is TimesToken || currentToken is DivideToken) {
            result = if (currentToken is TimesToken) {
                BinaryOperation(result, consume(), expr0())
            } else {
                BinaryOperation(result, consume(), expr0())
            }
        }

        return result
    }

    private fun expr2(): Expression {
        var result = expr1()

        while (currentToken is PlusToken || currentToken is MinusToken) {
            result = BinaryOperation(result, consume(), expr1())
        }

        return result
    }

    private fun expr3(): Expression {
        val result = expr2()

        if (currentToken !is LogicalOperator) {
            return result
        }

        return BinaryOperation(result, consume(), expr2())
    }

    private fun expression() : Expression {
        return expr3()
    }

    private inline fun<reified T: Token> consume(): T {
        if (currentToken is T) {
            val old = currentToken

            currentToken = lexer.next()
            return old as T
        }

        throw ParserException(if (currentToken is EOFToken) {
            "Unexpected end of file."
        } else {
            "Unexpected token. Expected ${T::class.simpleName} got ${currentToken::class.simpleName}"
        })
    }
}