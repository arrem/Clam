package io.gitlab.arrem.clam.parser

import io.gitlab.arrem.clam.analysis.Type
import io.gitlab.arrem.clam.builtins.Function
import io.gitlab.arrem.clam.lexer.AssignmentOperator
import io.gitlab.arrem.clam.lexer.BinaryOperator
import io.gitlab.arrem.clam.lexer.IdentifierToken
import io.gitlab.arrem.clam.lexer.UnaryOperator

interface ASTVisitor {
    fun visitProgram(node: Program): Any

    fun visitBlock(node: Block): Any
    fun visitIfStatement(node: IfStatement): Any
    fun visitWhileStatement(node: WhileStatement): Any

    fun visitFunctionDeclaration(node: FunctionDeclaration): Any
    fun visitArgument(node: Argument): Any
    fun visitReturnStatement(node: ReturnStatement): Any

    fun visitAssignment(node: Assignment): Any
    fun visitInitialization(node: Initialization): Any

    fun visitBinaryOperation(node: BinaryOperation): Any
    fun visitUnaryOperation(node: UnaryOperation): Any
    fun visitIdentifierExpression(node: IdentifierExpression): Any
    fun visitFunctionCall(node: FunctionCall): Any

    fun visitFloatSymbol(node: FloatSymbol): Any
    fun visitBooleanSymbol(node: BooleanSymbol): Any
}

interface AST {
    fun visit(visitor: ASTVisitor): Any
}

interface ProgramPart : AST
interface Statement : ProgramPart
interface Expression : Statement

class Program(val statements: List<ProgramPart>) : AST {
    override fun visit(visitor: ASTVisitor) = visitor.visitProgram(this)
    override fun toString(): String {
        return statements.joinToString("\n") {
            it.toString().split("\n").joinToString("") { "\n$it" }
        }
    }
}

class Block(val statements: List<Statement>) : Statement {
    override fun visit(visitor: ASTVisitor) = visitor.visitBlock(this)
    override fun toString(): String {
        val transformed = statements.joinToString("") {
            "\n\t" + it.toString().replace("\n", "\n\t")
        }
        return "block { $transformed \n}"
    }
}

class FunctionDeclaration(name: String, args: List<Argument>, returnType: Type, val body: Block) : ProgramPart,
        Function(name, args, returnType) {
    override fun visit(visitor: ASTVisitor) = visitor.visitFunctionDeclaration(this)
    override fun toString(): String {
        return "decl fun $name(${args.joinToString(",")}): $returnType = $body"
    }
}

class Argument(val name: String, val type: Type) : AST {
    override fun visit(visitor: ASTVisitor) = visitor.visitArgument(this)
    override fun toString(): String {
        return "arg($name: $type)"
    }
}

class ReturnStatement(val expression: Expression?) : Statement {
    override fun visit(visitor: ASTVisitor) = visitor.visitReturnStatement(this)
    override fun toString(): String {
        return "ret($expression)"
    }
}

class IfStatement(val condition: Expression, val trueStatement: Statement, val falseStatement: Statement? = null) : Statement {
    override fun visit(visitor: ASTVisitor) = visitor.visitIfStatement(this)
    override fun toString(): String {
        val append = if (falseStatement != null) {
            "\nelse\n\t${falseStatement.toString().replace("\n", "\n\t")})"
        } else {
            ""
        }

        return "if ($condition)\n\t${trueStatement.toString().replace("\n", "\n\t")})$append"
    }
}

class WhileStatement(val condition: Expression, val statement: Statement) : Statement {
    override fun visit(visitor: ASTVisitor) = visitor.visitWhileStatement(this)
    override fun toString(): String {
        return "while ($condition)\n\t${statement.toString().replace("\n", "\n\t")})"
    }
}

class FunctionCall(val name: String, val args: List<Expression>) : Expression {
    override fun visit(visitor: ASTVisitor) = visitor.visitFunctionCall(this)
    override fun toString(): String {
        return "fn~$name($args)"
    }
}

class Initialization(val id: IdentifierToken, val value: Expression) : Statement {
    override fun visit(visitor: ASTVisitor) = visitor.visitInitialization(this)
    override fun toString(): String {
        return "init(id~${id.value} = $value)"
    }
}

class Assignment(val id: IdentifierToken, val op: AssignmentOperator, val value: Expression) : Statement {
    override fun visit(visitor: ASTVisitor) = visitor.visitAssignment(this)
    override fun toString(): String {
        return "assign(id~${id.value} $op $value)"
    }
}

class BinaryOperation(val left: Expression, val op: BinaryOperator, val right: Expression) : Expression {
    override fun visit(visitor: ASTVisitor) = visitor.visitBinaryOperation(this)
    override fun toString(): String {
        return "binop($left $op $right)"
    }
}

class UnaryOperation(val op: UnaryOperator, val right: Expression) : Expression {
    override fun visit(visitor: ASTVisitor) = visitor.visitUnaryOperation(this)
    override fun toString(): String {
        return "unop($op $right)"
    }
}

class IdentifierExpression(val value: String) : Expression {
    override fun visit(visitor: ASTVisitor) = visitor.visitIdentifierExpression(this)
    override fun toString(): String {
        return "id~$value"
    }

}

class FloatSymbol(val value: Float) : Expression {
    override fun visit(visitor: ASTVisitor) = visitor.visitFloatSymbol(this)
    override fun toString(): String {
        return "float($value)"
    }
}

class BooleanSymbol(val value: Boolean) : Expression {
    override fun visit(visitor: ASTVisitor) = visitor.visitBooleanSymbol(this)
    override fun toString(): String {
        return "bool($value)"
    }
}