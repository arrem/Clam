package io.gitlab.arrem.clam

import io.gitlab.arrem.clam.analysis.SemanticChecker
import io.gitlab.arrem.clam.borealis.Generator
import io.gitlab.arrem.clam.interpreter.Interpreter
import io.gitlab.arrem.clam.lexer.Lexer
import io.gitlab.arrem.clam.parser.Parser
import java.io.File
import java.io.FileNotFoundException

const val VERSION = "1.0-SNAPSHOT"
var debug = false
var borealis = false

class InternalCompilerException(message: String) : RuntimeException(message)

fun main(array: Array<String>) {

    debug = array.contains("-debug")
    borealis = array.contains("-borealis")

    println("Clam REPL, version $VERSION")
    println("Type \"exit\" to quit\n")

    while (true) {
        print(">>> ")

        val line = readLine() ?: error("Could not read line.")

        if (line.trim() == "exit") {
            break
        }

        val input = if (line.startsWith(":")) {
            // Interpret input starting with : as a file name, and read the contents.
            val file = File(line.substring(1))
            try {
                file.readText()
            } catch (e: FileNotFoundException) {
                println("The file '${file.path}' could not be found.")
                continue
            }
        } else {
            // Otherwise interpret it as source code.
            line
        }

        interpret(input)
        println()
    }
}

fun Byte.toPositiveInt() = toInt() and 0xFF

fun interpret(input: String) {
    try {
        val lexer = Lexer(input)
        val parser = Parser(lexer)
        val checker = SemanticChecker()

        val program = parser.program()

        if (debug) {
            println(program)
        }

        program.visit(checker)

        val start = System.currentTimeMillis()
        if (borealis) {
            val generator = Generator(checker.declarations)
            program.visit(generator)

            if (debug) {
                println(generator.code.map { String.format("%02X", it.toPositiveInt()) })
            }

            File("D:\\Documents\\Uni\\4. Semester\\ABSM\\BorealisVM\\test\\curr.ab")
                    .writeBytes(generator.code.toByteArray())
        } else {
            val interpreter = Interpreter(checker.declarations)
            program.visit(interpreter)
        }
        val end = System.currentTimeMillis()
        println("Execution time: ${end - start}ms")

    } catch (e: Exception) {
        println("${e::class.simpleName}: ${e.message}")
        return
    }
}