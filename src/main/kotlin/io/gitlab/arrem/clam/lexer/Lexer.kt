package io.gitlab.arrem.clam.lexer

class LexerException(message: String) : RuntimeException(message)

class Lexer(input: String) {
    private var iter = input.asSequence()

    private val keywords = mapOf(
            "let"     to LetToken(),
            "if"      to IfToken(),
            "else"    to ElseToken(),
            "while"   to WhileToken(),
            "true"    to TrueToken(),
            "false"   to FalseToken(),
            "fn"     to FnToken(),
            "return"  to ReturnToken(),
            "float"   to FloatToken(),
            "bool" to BoolToken(),
            "unit"    to UnitToken()
    )

    private fun peek(): Char? {
        return iter.take(1).singleOrNull()
    }

    private fun advance(): Char? {
        val char = peek()
        iter = iter.drop(1)
        return char
    }

    private fun consumeWhitespace() {
        iter = iter.dropWhile(Char::isWhitespace)
    }

    private fun consumeComment() {
        iter = iter.dropWhile { it != '\n' }
    }

    private fun identifier(): Token {
        val result = iter.takeWhile { it.isLetter() || it == '_' }.joinToString("")
        iter = iter.drop(result.length)

        return keywords.getOrElse(result, { IdentifierToken(result) })
    }

    private fun float(): FloatLiteral {
        val characteristic = iter.takeWhile({ it in '0'..'9' }).joinToString("")
        iter = iter.drop(characteristic.length)

        if (peek() != '.') {
            return FloatLiteral(characteristic.toFloat())
        }

        advance()

        val mantissa = iter.takeWhile({ it in '0'..'9' }).joinToString("")
        iter = iter.drop(mantissa.length)

        return FloatLiteral("$characteristic.$mantissa".toFloat())
    }

    fun next(): Token {
        while (!iter.none()) {
            val char = peek()

            // If we've encountered whitespace, skip it all and run the loop again
            // to fetch the actual next token:
            if (char?.isWhitespace() == true) {
                consumeWhitespace()
                continue
            }

            if (char?.isLetter() == true) {
                return identifier()
            }

            if (char?.isDigit() == true) {
                return float()
            }

            if (char == '+') {
                advance()

                if (peek() == '=') {
                    advance()
                    return PlusAssignToken()
                }

                return PlusToken()
            }

            if (char == '-') {
                advance()

                if (peek() == '=') {
                    advance()
                    return MinusAssignToken()
                }

                return MinusToken()
            }

            if (char == '*') {
                advance()

                if (peek() == '=') {
                    advance()
                    return TimesAssignToken()
                }

                return TimesToken()
            }

            if (char == '/') {
                advance()

                if (peek() == '=') {
                    advance()
                    return DivideAssignToken()
                }

                if (peek() == '/') {
                    advance()
                    consumeComment()
                    continue
                }

                return DivideToken()
            }

            if (char == '(') {
                advance()
                return LParenToken()
            }

            if (char == ')') {
                advance()
                return RParenToken()
            }

            if (char == '{') {
                advance()
                return LBraceToken()
            }

            if (char == '}') {
                advance()
                return RBraceToken()
            }

            if (char == ';') {
                advance()
                return SemicolonToken()
            }

            if (char == ':') {
                advance()
                return ColonToken()
            }

            if (char == ',') {
                advance()
                return CommaToken()
            }

            if (char == '=') {
                advance()

                if (peek() == '=') {
                    advance()
                    return EqualsToken()
                }

                return AssignToken()
            }

            if (char == '<') {
                advance()

                if (peek() == '=') {
                    advance()
                    return LessThanOrEqualToken()
                }

                return LessThanToken()
            }

            if (char == '>') {
                advance()

                if (peek() == '=') {
                    advance()
                    return GreaterThanOrEqualToken()
                }

                return GreaterThanToken()
            }

            throw LexerException("Unexpected character '$char'")
        }

        return EOFToken()
    }
}