package io.gitlab.arrem.clam.lexer

import io.gitlab.arrem.clam.analysis.Type

interface Token
abstract class BinaryOperator(val operandType: Type, val resultType: Type) : Token
abstract class ArithmeticOperator : BinaryOperator(Type.Float, Type.Float)
abstract class LogicalOperator : BinaryOperator(Type.Float, Type.Boolean)
interface UnaryOperator : Token
interface AssignmentOperator : Token
abstract class TypeDeclaration(val type: Type): Token
abstract class BooleanLiteral(val value: Boolean) : Token

// Values and identifiers
class IdentifierToken(val value: String) : Token

class FloatLiteral(val value: Float) : Token

// Keywords
class LetToken : Token

class TrueToken : BooleanLiteral(true)

class FalseToken : BooleanLiteral(false)

class IfToken : Token

class ElseToken : Token

class WhileToken : Token

class FnToken : Token

class ReturnToken : Token

class FloatToken : TypeDeclaration(Type.Float)

class BoolToken : TypeDeclaration(Type.Boolean)

class UnitToken : TypeDeclaration(Type.Unit)

// Operators
class PlusToken : ArithmeticOperator(), UnaryOperator {
    override fun toString() = "+"
}

class MinusToken : ArithmeticOperator(), UnaryOperator {
    override fun toString() = "-"
}

class TimesToken : ArithmeticOperator() {
    override fun toString() = "*"
}

class DivideToken : ArithmeticOperator() {
    override fun toString() = "/"
}

class AssignToken : AssignmentOperator {
    override fun toString() = "="
}

class PlusAssignToken : AssignmentOperator {
    override fun toString() = "+="
}

class MinusAssignToken : AssignmentOperator {
    override fun toString() = "-="
}

class TimesAssignToken : AssignmentOperator {
    override fun toString() = "*="
}

class DivideAssignToken : AssignmentOperator {
    override fun toString() = "/="
}

class LessThanToken : LogicalOperator() {
    override fun toString() = "<"
}

class GreaterThanToken : LogicalOperator() {
    override fun toString() = ">"
}

class EqualsToken : LogicalOperator() {
    override fun toString() = "=="
}

class LessThanOrEqualToken : LogicalOperator() {
    override fun toString() = "<="
}

class GreaterThanOrEqualToken : LogicalOperator() {
    override fun toString() = ">="
}

// Symbols
class LParenToken : Token

class RParenToken : Token

class SemicolonToken : Token

class ColonToken : Token

class CommaToken : Token

class LBraceToken : Token

class RBraceToken : Token

// Special
class EOFToken : Token