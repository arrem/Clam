package io.gitlab.arrem.clam.analysis

import io.gitlab.arrem.clam.parser.*

data class DeclarationInfo(
        val name: String,
        val args: List<Argument>, val returnType: Type,
        val body: Block,
        val locals: Int
) {
    var addressStart: Int = 0
}

class DeclarationResolver : ASTVisitor {
    private val declarations = mutableListOf<DeclarationInfo>()
    
    override fun visitProgram(node: Program): List<DeclarationInfo> {
        node.statements.forEach { it.visit(this) }

        return declarations
    }

    override fun visitBlock(node: Block): Int {
        return node.statements.map { it.visit(this) as? Int ?: 0 }.sum()
    }

    override fun visitIfStatement(node: IfStatement) { }

    override fun visitWhileStatement(node: WhileStatement) { }

    override fun visitFunctionDeclaration(node: FunctionDeclaration) {
        val locals = node.body.visit(this) as Int
        declarations.add(DeclarationInfo(node.name, node.args, node.returnType, node.body, locals))
    }

    override fun visitArgument(node: Argument) { }

    override fun visitReturnStatement(node: ReturnStatement) { }

    override fun visitAssignment(node: Assignment) { }

    override fun visitInitialization(node: Initialization) = 1

    override fun visitBinaryOperation(node: BinaryOperation) { }

    override fun visitUnaryOperation(node: UnaryOperation) { }

    override fun visitIdentifierExpression(node: IdentifierExpression) { }

    override fun visitFunctionCall(node: FunctionCall) { }

    override fun visitFloatSymbol(node: FloatSymbol) { }

    override fun visitBooleanSymbol(node: BooleanSymbol) { }

}