package io.gitlab.arrem.clam.analysis

import io.gitlab.arrem.clam.InternalCompilerException
import io.gitlab.arrem.clam.builtins.Function
import io.gitlab.arrem.clam.builtins.builtins
import io.gitlab.arrem.clam.interpreter.Scope
import io.gitlab.arrem.clam.parser.*

class SemanticsException(message: String) : RuntimeException(message)


enum class Type {
    Float,
    Boolean,
    Unit
}

class SemanticChecker : ASTVisitor {
    var currentScope = Scope<Type>()

    private val topLevel = currentScope
    private val functions = mutableMapOf<String, Function>()

    val declarations = mutableListOf<DeclarationInfo>()

    private val inferenceVisitor = TypeInference(this)

    init {
        builtins.values.forEach {
            currentScope[it.name] = it.returnType
            functions[it.name] = it
        }
    }

    override fun visitProgram(node: Program) {
        declarations.addAll(DeclarationResolver().visitProgram(node))

        declarations.forEach {
            currentScope[it.name] = it.returnType
            functions[it.name] = FunctionDeclaration(it.name, it.args, it.returnType, it.body)
        }

        node.statements.forEach { it.visit(this) }
    }

    override fun visitBlock(node: Block) {
        currentScope = Scope(currentScope)

        node.statements.forEach { it.visit(this) }
        currentScope = currentScope.parent!!
    }

    override fun visitFunctionDeclaration(node: FunctionDeclaration) {
        val old = currentScope
        currentScope = Scope(topLevel)

        // Add function arguments into the current scope.
        node.args.forEach({ currentScope.setDirect(it.name, it.type) })

        node.body.statements.forEach({
            it.visit(this)

            if (it is ReturnStatement) {
                val type = it.expression?.visit(inferenceVisitor) ?: Type.Unit
                if (type != node.returnType) {
                    throw TypeException("Return type $type different than function return type for ${node.name}.")
                }
            }
        })

        // TODO: always return analysis

        currentScope = old
    }

    override fun visitArgument(node: Argument) { }

    override fun visitReturnStatement(node: ReturnStatement) { }

    override fun visitIfStatement(node: IfStatement) {
        if (node.condition.visit(inferenceVisitor) != Type.Boolean) {
            throw TypeException("Expected ${Type.Boolean} in if condition.")
        }

        node.condition.visit(this)

        node.trueStatement.visit(this)
        node.falseStatement?.visit(this)
    }

    override fun visitWhileStatement(node: WhileStatement) {
        if (node.condition.visit(inferenceVisitor) != Type.Boolean) {
            throw TypeException("Expected ${Type.Boolean} in while condition.")
        }

        node.condition.visit(this)

        node.statement.visit(this)
    }

    override fun visitAssignment(node: Assignment) {
        val ident = node.id.value

        val tableEntry = currentScope[ident] ?:
            throw SemanticsException("Variable $ident assigned before initialization.")

        node.value.visit(this)

        val newType = node.value.visit(inferenceVisitor)

        if (tableEntry != newType) {
            throw TypeException("Expected $tableEntry, got $newType for assignment to `$ident`.")
        }
    }

    override fun visitInitialization(node: Initialization) {
        val ident = node.id.value

        if (currentScope.containsDirect(ident)) {
            throw SemanticsException("Variable with the name `$ident` already initialized.")
        }

        node.value.visit(this)

        currentScope[ident] = node.value.visit(inferenceVisitor) as Type
    }

    override fun visitBinaryOperation(node: BinaryOperation) {
        node.left.visit(this)
        node.right.visit(this)
    }

    override fun visitUnaryOperation(node: UnaryOperation) {
        node.right.visit(this)
    }

    override fun visitIdentifierExpression(node: IdentifierExpression) {
        if (node.value !in currentScope) {
            throw SemanticsException("Variable `${node.value}` referenced before initialization.")
        }
    }

    override fun visitFunctionCall(node: FunctionCall) {
        if (node.name !in currentScope) {
            throw SemanticsException("Function `${node.name}` referenced before initialization.")
        }

        val function = functions[node.name] ?:
        throw InternalCompilerException("Undefined function `${node.name}`.")

        if (node.args.size != function.args.size) {
            throw SemanticsException(
                    "Function arity mismatch. Expected ${function.args.size} arguments, got ${node.args.size}"
            )
        }

        node.args.map { it.visit(inferenceVisitor) as Type }.zip(function.args).forEachIndexed {
            i, (actual, expected) ->
            if (actual != expected.type) {
                throw SemanticsException("Expected ${expected.type} as argument #${i + 1}, got $actual.")
            }
        }
    }

    override fun visitFloatSymbol(node: FloatSymbol) { }

    override fun visitBooleanSymbol(node: BooleanSymbol) { }
}