package io.gitlab.arrem.clam.analysis

import io.gitlab.arrem.clam.InternalCompilerException
import io.gitlab.arrem.clam.parser.*

class TypeException(message: String) : RuntimeException(message)

class TypeInference(private val semanticChecker: SemanticChecker) : ASTVisitor {
    private val currentScope
        get() = semanticChecker.currentScope

    override fun visitProgram(node: Program) {
        throw InternalCompilerException("Type inference visitor used on ${node::class.simpleName} node.")
    }

    override fun visitBlock(node: Block) {
        throw InternalCompilerException("Type inference visitor used on ${node::class.simpleName} node.")
    }

    override fun visitIfStatement(node: IfStatement) {
        throw InternalCompilerException("Type inference visitor used on ${node::class.simpleName} node.")
    }

    override fun visitWhileStatement(node: WhileStatement) {
        throw InternalCompilerException("Type inference visitor used on ${node::class.simpleName} node.")
    }

    override fun visitAssignment(node: Assignment) {
        throw InternalCompilerException("Type inference visitor used on ${node::class.simpleName} node.")
    }

    override fun visitInitialization(node: Initialization) {
        throw InternalCompilerException("Type inference visitor used on ${node::class.simpleName} node.")
    }

    override fun visitFunctionDeclaration(node: FunctionDeclaration): Type {
        return node.returnType
    }

    override fun visitArgument(node: Argument): Any {
        return node.type
    }

    override fun visitReturnStatement(node: ReturnStatement): Any {
        return node.expression?.visit(this) ?: Type.Unit
    }

    override fun visitBinaryOperation(node: BinaryOperation): Type {
        val typeLeft = node.left.visit(this)
        val typeRight = node.right.visit(this)

        if (typeLeft != node.op.operandType) {
            throw TypeException("Expected ${node.op.operandType} as left operand for ${node.op}, got $typeLeft.")
        }

        if (typeRight != node.op.operandType) {
            throw TypeException("Expected ${node.op.operandType} as right operand for ${node.op}, got $typeLeft.")
        }

        return node.op.resultType
    }

    override fun visitUnaryOperation(node: UnaryOperation): Type {
        val type = node.right.visit(this)

        if (type == Type.Float) {
            return Type.Float
        }

        throw TypeException("Invalid type in unary expression. Expected ${Type.Float} got $type.")
    }

    override fun visitIdentifierExpression(node: IdentifierExpression): Type {
        return currentScope[node.value] ?:
                throw SemanticsException("Variable `${node.value}` referenced before initialization.")
    }

    override fun visitFunctionCall(node: FunctionCall): Type {
        return currentScope[node.name] ?:
                throw SemanticsException("Function `${node.name}` referenced before initialization.")
    }

    override fun visitFloatSymbol(node: FloatSymbol) = Type.Float

    override fun visitBooleanSymbol(node: BooleanSymbol) = Type.Boolean
}