package io.gitlab.arrem.clam.borealis

import io.gitlab.arrem.clam.analysis.DeclarationInfo
import io.gitlab.arrem.clam.analysis.Type
import io.gitlab.arrem.clam.builtins.builtins
import io.gitlab.arrem.clam.interpreter.Scope
import io.gitlab.arrem.clam.lexer.*
import io.gitlab.arrem.clam.parser.*
import kotlin.math.roundToInt

class Generator(declarations: List<DeclarationInfo>) : ASTVisitor {

    val code = mutableListOf<Byte>()
    private val pc
        get() = code.size

    private val declarations = mutableListOf<DeclarationInfo>()

    private var headerSize = 0

    private var localOffset = 0

    private var varMap = Scope<Int>()
    private var maxVar = 0

    init {
        this.declarations.addAll(declarations)

        // Magic number.
        code.addAll(arrayOf<Byte>(0x3D, 0x62, 0x16, 0x62))
        // Version.
        code.addAll(arrayOf<Byte>(0x00, 0x08))
        // Header end.
        code.add(0xFA.toByte())
    }

    override fun visitProgram(node: Program) {
        val statements = Block(
                node.statements
                        .filter { it !is FunctionDeclaration }
                        .map { it as Statement }
                        .toMutableList()
        )

        (statements.statements as MutableList<Statement>).add(ReturnStatement(null))

        val locals = statements.statements.filter { it is Initialization }.count()

        declarations.add(0, DeclarationInfo("main", listOf(), Type.Unit, statements, locals))

        declarations.forEach {
            code.addAll(arrayOf(
                    // Declaration start.
                    0xD0.toByte(),
                    // Number of locals (includes args).
                    (it.locals + it.args.size).toByte(),
                    // Number of args.
                    it.args.size.toByte(),
                    // Reserve bytes for starting address, fill in later.
                    0, 0, 0, 0,
                    // Declaration end.
                    0xDE.toByte()
            ))

            // Set a reference to the index of the starting address bytes, so we can
            // fill them in more easily.
            it.addressStart = code.size - 5
        }

        // Declaration list end byte.
        code.add(0xDF.toByte())

        headerSize = code.size

        declarations
                .map { FunctionDeclaration(it.name, it.args, it.returnType, it.body) }
                .forEach {
                    it.visit(this)
                }
    }

    override fun visitBlock(node: Block) {
        varMap = Scope(varMap)

        node.statements.forEach { it.visit(this); }

        varMap = varMap.parent!!
    }

    override fun visitIfStatement(node: IfStatement) {
        node.condition.visit(this)

        gen(Instruction.JmpZero, 0)
        val p1 = pc - 1

        node.trueStatement.visit(this)

        if (node.falseStatement != null) {
            gen(Instruction.Jmp, 0)
            val p2 = pc - 1

            code[p1] = (pc - localOffset).toByte()

            node.falseStatement.visit(this)

            code[p2] = (pc - localOffset + 1).toByte()
        }

        code[p1] = (pc - localOffset).toByte()
    }

    override fun visitWhileStatement(node: WhileStatement) {
        val p1 = pc

        node.condition.visit(this)

        gen(Instruction.JmpZero, 0)

        val p2 = pc - 1

        node.statement.visit(this)

        gen(Instruction.Jmp, (p1 - localOffset).toByte())

        code[p2] = (pc - localOffset).toByte()
    }

    override fun visitFunctionDeclaration(node: FunctionDeclaration) {
        val pos = code.size - headerSize
        val start = declarations.find { it.name == node.name }?.addressStart ?: TODO("handle")

        code[start] = ((pos and (-16777216)) ushr 24).toByte()
        code[start + 1] = ((pos and (16711680)) ushr 16).toByte()
        code[start + 2] = ((pos and (65280)) ushr 8).toByte()
        code[start + 3] = (pos and (255)).toByte()

        localOffset = code.size
        maxVar = node.args.size

        varMap = Scope(varMap)

        for (i in (0 until node.args.size)) {
            varMap.setDirect(node.args[i].name, i)
        }

        node.body.visit(this)

        varMap = varMap.parent!!
    }

    override fun visitArgument(node: Argument) {

    }

    override fun visitReturnStatement(node: ReturnStatement) {
        node.expression?.visit(this)
        gen(Instruction.Return)
    }

    override fun visitAssignment(node: Assignment) {
        // Special fast assignment operator is applicable.
        if (node.value is FloatSymbol) {
            val id = varMap[node.id.value]!!.toByte()
            val value = node.value.value
            val useByte = value.roundToInt().toFloat() == value && value <= 255

            when (node.op) {
                is AssignToken ->
                        if (useByte) {
                            gen(Instruction.VarBCSet, id, value.toByte())
                        } else {
                            gen(Instruction.VarFCSet, id, *floatToBytes(value))
                        }
                is PlusAssignToken ->
                    if (useByte) {
                        gen(Instruction.VarBCAdd, id, value.toByte())
                    } else {
                        gen(Instruction.VarFCAdd, id, *floatToBytes(value))
                    }
                is MinusAssignToken ->
                    if (useByte) {
                        gen(Instruction.VarBCSub, id, value.toByte())
                    } else {
                        gen(Instruction.VarFCSub, id, *floatToBytes(value))
                    }
                is TimesAssignToken ->
                    if (useByte) {
                        gen(Instruction.VarBCMul, id, value.toByte())
                    } else {
                        gen(Instruction.VarFCMul, id, *floatToBytes(value))
                    }
                is DivideAssignToken ->
                    if (useByte) {
                        gen(Instruction.VarBCDiv, id, value.toByte())
                    } else {
                        gen(Instruction.VarFCDiv, id, *floatToBytes(value))
                    }
                else ->
                        error("Unsupported assignment operator.")
            }

            return
        }

        node.value.visit(this)

        val id = varMap[node.id.value]

        gen(Instruction.FStore, id!!.toByte())
    }

    override fun visitInitialization(node: Initialization) {
        varMap.setDirect(node.id.value, maxVar)

        // Special fast assignment operator is applicable
        if (node.value is FloatSymbol) {
            val value = node.value.value

            if (value.roundToInt().toFloat() == value && value <= 255) {
                gen(Instruction.VarBCSet, maxVar.toByte(), value.toInt().toByte())
            } else {
                gen(Instruction.VarFCSet, maxVar.toByte(), *floatToBytes(value))
            }

            maxVar++
            return
        }

        node.value.visit(this)

        gen(Instruction.FStore, maxVar.toByte())

        maxVar++
    }

    override fun visitBinaryOperation(node: BinaryOperation) {
        node.left.visit(this)
        node.right.visit(this)

        when (node.op) {
            is PlusToken ->
                    gen(Instruction.FAdd)
            is MinusToken ->
                    gen(Instruction.FSub)
            is TimesToken ->
                    gen(Instruction.FMul)
            is DivideToken ->
                    gen(Instruction.FDiv)
            is EqualsToken ->
                    gen(Instruction.FCmpEq)
            is LessThanToken ->
                    gen(Instruction.FCmpLe)
            is LessThanOrEqualToken ->
                    gen(Instruction.FCmpLeq)
            is GreaterThanToken ->
                    gen(Instruction.FCmpGe)
            is GreaterThanOrEqualToken ->
                    gen(Instruction.FCmpGeq)
            else ->
                    error("Unsupported binary operator.")
        }
    }

    override fun visitUnaryOperation(node: UnaryOperation) {
        node.right.visit(this)

        when (node.op) {
            is PlusToken -> return
            is MinusToken ->
                    gen(Instruction.FNeg)
        }
    }

    override fun visitIdentifierExpression(node: IdentifierExpression) {
        val id = varMap[node.value]
        gen(Instruction.FLoad, id!!.toByte())
    }

    override fun visitFunctionCall(node: FunctionCall) {
        // Built in function handling:
        if (builtins.containsKey(node.name)) {
            node.args.forEach { it.visit(this) }
            gen(Instruction.BInvoke, builtins.keys.indexOf(node.name).toByte())

            return
        }

        // User defined function handling:
        node.args.forEach { it.visit(this) }
        gen(Instruction.Invoke, declarations.indexOfFirst { it.name == node.name }.toByte())
    }

    override fun visitFloatSymbol(node: FloatSymbol) {
        val value = node.value

        if (value.roundToInt().toFloat() == value && value <= 255) {
            gen(Instruction.BPush, value.toInt().toByte())
        } else {
            gen(Instruction.FPush, *floatToBytes(value))
        }
    }

    private fun floatToBytes(value: Float): ByteArray {
        val bits = value.toRawBits()

        val b1 = ((bits and (-16777216)) ushr 24).toByte()
        val b2 = ((bits and (16711680)) ushr 16).toByte()
        val b3 = ((bits and (65280)) ushr 8).toByte()
        val b4 = (bits and (255)).toByte()

        return arrayOf(b1, b2, b3, b4).toByteArray()
    }

    override fun visitBooleanSymbol(node: BooleanSymbol) {
        gen(Instruction.BPush, if (node.value) { 1 } else { 0 })
    }

    private fun gen(instruction: Instruction, vararg params: Byte) {
        code.add(instruction.value.toByte())
        params.forEach { code.add(it) }
    }
}