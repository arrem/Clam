package io.gitlab.arrem.clam.borealis

enum class Instruction(val value: Short) {
    BPush(0),
    FPush(1),
    FStore(2),
    FLoad(3),

    FAdd(0x0A),
    FSub(0x0B),
    FMul(0x0C),
    FDiv(0x0D),
    FNeg(0x0E),

    VarBCSet(0x0F),
    VarBCAdd(0x10),
    VarBCSub(0x11),
    VarBCMul(0x12),
    VarBCDiv(0x13),
    VarFCSet(0x14),
    VarFCAdd(0x15),
    VarFCSub(0x16),
    VarFCMul(0x17),
    VarFCDiv(0x18),

    FCmpEq(0x1E),
    FCmpNeq(0x1F),
    FCmpLe(0x20),
    FCmpLeq(0x21),
    FCmpGe(0x22),
    FCmpGeq(0x23),

    Jmp(0x28),
    JmpOne(0x29),
    JmpZero(0x2A),

    Invoke(0x32),
    BInvoke(0x33),
    Return(0x34),
}