package io.gitlab.arrem.clam.interpreter

class Scope<T>(val parent: Scope<T>? = null) {
    private val values = mutableMapOf<String, T>()

    fun containsDirect(identifier: String) = values.containsKey(identifier)

    operator fun contains(identifier: String): Boolean =
            containsDirect(identifier) || parent?.contains(identifier) == true

    operator fun get(identifier: String): T? {
        return values[identifier] ?: parent?.get(identifier)
    }

    operator fun set(identifier: String, value: T) {
        if (identifier !in values) {
            if (parent == null) {
                values[identifier] = value
                return
            }

            if (parent.contains(identifier)) {
                parent[identifier] = value
                return
            }
        }

        values[identifier] = value
    }

    fun setDirect(identifier: String, value: T) {
        values[identifier] = value
    }

    override fun toString() = values.toString()
}