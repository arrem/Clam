package io.gitlab.arrem.clam.interpreter

import io.gitlab.arrem.clam.InternalCompilerException
import io.gitlab.arrem.clam.analysis.DeclarationInfo
import io.gitlab.arrem.clam.analysis.Type
import io.gitlab.arrem.clam.builtins.builtins
import io.gitlab.arrem.clam.lexer.*
import io.gitlab.arrem.clam.parser.*

class Return(val value: Any?) : RuntimeException()

class Interpreter(declarations: List<DeclarationInfo>) : ASTVisitor {
    private var currentScope = Scope<Any>()
    private val topLevel = currentScope

    init {
        declarations.forEach {
            currentScope[it.name] = FunctionDeclaration(it.name, it.args, it.returnType, it.body)
        }
    }

    override fun visitProgram(node: Program) {
        node.statements.forEach { it.visit(this) }
    }

    override fun visitBlock(node: Block) {
        currentScope = Scope(currentScope)

        node.statements.forEach { it.visit(this) }

        currentScope = currentScope.parent!!
    }

    override fun visitFunctionDeclaration(node: FunctionDeclaration) {
        currentScope[node.name] = node
    }

    override fun visitArgument(node: Argument) { }

    override fun visitReturnStatement(node: ReturnStatement): Any {
        throw Return(node.expression?.visit(this))
    }

    override fun visitIfStatement(node: IfStatement) {
        if (expect(node.condition.visit(this))) {
            node.trueStatement.visit(this)
        } else {
            node.falseStatement?.visit(this)
        }
    }

    override fun visitWhileStatement(node: WhileStatement) {
        while (expect(node.condition.visit(this))) {
            node.statement.visit(this)
        }
    }

    override fun visitAssignment(node: Assignment) {
        when (node.op) {
            is AssignToken -> currentScope[node.id.value] =
                    node.value.visit(this)
            is PlusAssignToken -> currentScope[node.id.value] =
                    expect<Float>(currentScope[node.id.value]) + expect<Float>(node.value.visit(this))
            is MinusAssignToken -> currentScope[node.id.value] =
                    expect<Float>(currentScope[node.id.value]) - expect<Float>(node.value.visit(this))
            is TimesAssignToken -> currentScope[node.id.value] =
                    expect<Float>(currentScope[node.id.value]) * expect<Float>(node.value.visit(this))
            is DivideAssignToken -> currentScope[node.id.value] =
                    expect<Float>(currentScope[node.id.value]) / expect<Float>(node.value.visit(this))
            else -> throw InternalCompilerException("Unsupported assignment operator '${node.op}'.")
        }
    }

    override fun visitInitialization(node: Initialization) {
        currentScope.setDirect(node.id.value, node.value.visit(this))
    }

    override fun visitBinaryOperation(node: BinaryOperation): Any {
        return when (node.op) {
            is PlusToken ->
                    expect<Float>(node.left.visit(this)) + expect<Float>(node.right.visit(this))
            is MinusToken ->
                expect<Float>(node.left.visit(this)) - expect<Float>(node.right.visit(this))
            is TimesToken ->
                expect<Float>(node.left.visit(this)) * expect<Float>(node.right.visit(this))
            is DivideToken ->
                expect<Float>(node.left.visit(this)) / expect<Float>(node.right.visit(this))
            is EqualsToken ->
                expect<Float>(node.left.visit(this)) == expect<Float>(node.right.visit(this))
            is LessThanToken ->
                expect<Float>(node.left.visit(this)) < expect<Float>(node.right.visit(this))
            is GreaterThanToken ->
                expect<Float>(node.left.visit(this)) > expect<Float>(node.right.visit(this))
            is LessThanOrEqualToken ->
                expect<Float>(node.left.visit(this)) <= expect<Float>(node.right.visit(this))
            is GreaterThanOrEqualToken ->
                expect<Float>(node.left.visit(this)) >= expect<Float>(node.right.visit(this))
            else ->
                throw InternalCompilerException("Unsupported binary operator '${node.op}'.")
        }
    }

    override fun visitUnaryOperation(node: UnaryOperation): Float {
        return when (node.op) {
            is PlusToken -> +(expect<Float>(node.right.visit(this)))
            is MinusToken -> -(expect<Float>(node.right.visit(this)))
            else          -> throw InternalCompilerException("Unsupported unary operator '${node.op}'.")
        }
    }

    override fun visitIdentifierExpression(node: IdentifierExpression): Any {
        return currentScope[node.value] ?: throw InternalCompilerException("Referenced undeclared variable.")
    }

    override fun visitFunctionCall(node: FunctionCall): Any {
        if (node.name in builtins) {
            val function = builtins[node.name] ?:
            throw InternalCompilerException("Referenced undeclared function.")

            return function.call(node.args.map { it.visit(this) })
        }

        val function = (currentScope[node.name] as? FunctionDeclaration) ?:
                throw InternalCompilerException("Referenced undeclared function.")

        // Resolve parameter bindings first.
        val bindings = node.args.zip(function.args).map {
            (param, spec) ->
            spec.name to param.visit(this)
        }

        // Create new scope to bind parameter values. This is a dirty hack but it works.
        val old = currentScope
        currentScope = Scope(topLevel)

        bindings.forEach {
            (name, value) -> currentScope.setDirect(name, value)
        }

        var returned: Any = Unit

        try {
            function.body.visit(this)
        } catch (ret: Return) {
            returned = ret.value ?: Unit
        }

        // Throw away temp scope after function execution is done.
        currentScope = old

        // This will be a hard compiler error in the future.
        if (returned == Unit && function.returnType != Type.Unit) {
            throw InternalCompilerException("Mmm we don't support this yet sorry. Fix your return type manually.")
        }

        return returned
    }

    override fun visitFloatSymbol(node: FloatSymbol): Float {
        return node.value
    }

    override fun visitBooleanSymbol(node: BooleanSymbol): Boolean {
        return node.value
    }

    private inline fun<reified T> expect(expr: Any?): T {
        if (expr is T) {
            return expr
        }

        throw InternalCompilerException("Type mismatch. Expected ${T::class::simpleName} got ${expr!!::class.simpleName
                ?: "null"}")
    }
}