# Clam
A toy programming language for an Abstract Machines course at TU Vienna, 2018.

Syntax example:
```rust
// Monte Carlo approximation of PI
fn square(n: float): float {
    return n * n;
}

let points_in = 0;
let total = 0;
let iterations = 100000;

while iterations > 0 {
    let x = random();
    let y = random();

    if square(x) + square(y) <= 1 {
        points_in += 1;
    }

    total += 1;
    iterations -= 1;
}

let pi = 4 * (points_in / total);
println(pi);
```

## Reading the code

This repository contains the source code for the Clam AST-interpreter, as well as the Borealis-VM backend, which
compiles the code to the bytecode, compatible with Clam's sister project
[BorealisVM](https://gitlab.com/arrem/BorealisVM). This section covers the design details of the language, the
interpreter and the bytecode compiler, aiming to guide interested readers in the right direction. We cover each of these
briefly and point the reader to interesting parts of the code.

### Clam language design

Clam was designed to be easy to parse and interpret. Syntactically it draws inspiration from Java and Rust, while
keeping the feature-set small enough for it to be easy to implement. The grammar
of the language (described in detail in [GRAMMAR.md](https://gitlab.com/arrem/Clam/blob/master/GRAMMAR.md)) is designed
to be LL(1), making it entirely parsable with a recursive descent parser.

The syntax of the language has already been shown, but we also give a brief overview of the language's supported
features:
 * Floating point data types, with arithmetic.
 * Boolean data types for conditionals.
 * `if`-`else` statements.
 * `while` statements.
 * Built-in functions. (Currently `println(float)` and `random()`)
 * User-defined functions.
 
Apart from these, inspired by Rust and the latest version of Java, the language features local type inference. All
variables are declared with the `let` keyword and the type is inferred automatically. Nevertheless, the language is
strongly typed and automatic casting between types is also not done, to guarantee type-safety.

### Interpreter design considerations

This section covers some of the design decisions that we have made for this project, and tries to explain the reasoning
behind them. We only cover the theoretical aspects in this section, while the next section deals with the actual
implementation and explains where the files can be found in the project.

As mentioned in the previous section, the language is using a [LL(1)](https://en.wikipedia.org/wiki/LL_parser) grammar,
which allows us to parse it with a [recursive descent parser](https://en.wikipedia.org/wiki/Recursive_descent_parser).
While not as powerful as others (e.g. [shift-reduce parsers](https://en.wikipedia.org/wiki/Shift-reduce_parser)), our
chosen method allows us to easily "translate" our grammar into a set of (mutually) recursive functions, as such there is
a clear connection between the language's grammar and the parser code, which also makes the grammar serve as a form of
documentation.

For many of the other operations, we make heavy use of the
[visitor pattern](https://en.wikipedia.org/wiki/Visitor_pattern). The semantic analysis, which verifies the
correctness of the program, the type inference and even the parsing and compiling to bytecode are all done using
visitors, which are defined for every node of the abstract syntax tree. This of course makes both the compiler and the
interpreter multi-pass. We chose the visitor pattern because it provides a uniform and easily adaptable interface for
working with the abstract syntax tree, which can be heavily customized to make it accomplish all of the tasks we listed
above. Without it, specialized handlers would have had to be written for each mentioned task.

The interpretation itself works according to the following pipeline:

![Lex, parse, analyze and infer types, run](https://i.imgur.com/oKW2u1a.png)

The run stage of the pipeline depends on the backend being used. The language can either be interpreted using the AST or
compiled into bytecode. Both operations are represented by this "abstract" pipeline stage.

Another interesting thing to note is that the type inference occurs parallel with the semantic analysis and not before
it. This is done because the full type information isn't needed immediately, so it's done lazily, as the types are
encountered by the semantic analyzer. The function declaration analysis is done before the semantic analysis starts,
since the function parameters and return types are not inferred automatically. Although this would in theory
be a possibility, we choose not to do so, since a functions's type information serves as a very good form of
documentation. Such a decision was also inspired by the way Rust handles its type inference.

The project is also unit-tested. The coverage, however, cannot be correctly determined, since the report generator has
trouble keeping track of Kotlin's inline functions, which this project makes extensive use of. Because of this, tooling
reports a lower test coverage. We can only hope to see a fix soon.

### Reading the project

#### Liftoff

The main body of the program is in `Main.kt`. It's responsible for reading the user input and running it through the
interpreter pipeline. With the command line argument `--debug`, the user can enable debug mode, which prints useful
debugging information, such as the parsed AST. With the `--borealis` flag, the user can switch the mode from the
AST-interpreter to the bytecode compiler.

#### Lexer

The lexer is responsible for breaking the user input up into tokens that are easy to parse. Some tokens, like keywords,
are only an object. Others, like number literals, also have attributes attached to them. The `lexer` package contains
the files necessary for the lexer. The definitions of the token classes are in the `Token.kt` class. In languages like
C, tokens are usually represented with enums, or tagged unions. Here, we choose to represent each token type with a
class. This, together with subclass relationships, makes the token handling code a lot cleaner, but also means that
we produce a lot of classes and tiny objects.

`Lexer.kt` is where the lexing happens. The input stream is tokenized lazily, with the `next()` function returning the
next token to consume. This method will skip any whitespace and comments, and use longest input match to find and return
the token. Another common approach for lexing is to use regular expressions, which we avoid in this project. Given the
relatively simple syntax, it is easier and cleaner to lex the input "by hand". While lexing, we make use of Kotlin's
Sequences, which allow for a functional-style approach, with functions like `drop` and `takeWhile`. This is how we
achieve a longest input match.

#### Parser

The `parser` package also defines the AST nodes, located in the `AST.kt` file. AST nodes implement the interface AST,
which provides them with a `visit` function that is used to implement the visitor pattern. An interesting observation is
that the function returns the type `Any` (the root of Kotlin's class hierarchy), but since the language permits
covariant return types, implementors ar free to determine the return type of the function themselves. This is done
because the return type depends on the visitor that's being implemented. For example, the type inference visitor might
want to return the inferred type of a variable, while other visitors like the semantic analyzer and the interpreter
don't make use of any particular return types.

As mentioned in the preceding section, the parser we use is a recursive descent parser. The functions match the names
of the productions defined in [GRAMMAR.md](https://gitlab.com/arrem/Clam/blob/master/GRAMMAR.md) and we assume basic
familiarity with the grammar in this section.

To allow for lazy lexing, the parser takes a reference to the `Lexer` object. Its next method is then called as-needed
to avoid lexing the whole input if we've already discovered a syntax error at one point in our parser.

Because of Kotlin's
[reified type parameters](https://kotlinlang.org/docs/reference/inline-functions.html#reified-type-parameters) our
`consume` function (located at the very bottom of `Parser.kt`) can be implemented in an elegant way. It checks to see if
the next token is an instance (or a subclass) of the expected token class and if so, it casts it to the correct class
and returns it to the caller.

Among the interesting functions here is the expression parsing function, which, as specified in the grammar, uses
multiple mutually recursive productions to handle operator precedence correctly. Because of this, no special operator
precedence table or special expression handling code is needed. The AST is guaranteed to be in the correct form.

As a demonstration of how elegantly a grammar translates into a recursive descent parser, we also showcase the function
that parses while loops.

```
while_statement = "while" , expression , statement
```

```kotlin
private fun whileStatement(): WhileStatement {
    consume<WhileToken>()

    val condition = expression()
    val statement = statement()

    return WhileStatement(condition, statement)
}
```

#### Semantic analyzer

In this section we encounter the three most important visitors of the project. In the `analysis` package, we have the
`DeclarationResolver` which is responsible for scanning through the file and generating the information about all of
the declarations in it. This is done so we can call make functions visible throughout the program, regardless of where
they are declared, as well as do the semantic analysis correctly. For the Borealis backend, we also gather information
about the total number of local variables declared within the functions.

We move onto the surprisingly least complicated visitor, the `TypeInference` visitor. Calling it on most nodes is
considered to be a compiler bug, since it should only be used on nodes that have types associated to them.

Here we highlight Kotlin's null handling. Kotlin provides
[null-safety](https://kotlinlang.org/docs/reference/null-safety.html#safe-calls) by supporting null-safe calls and the
elvis operator. Our return statement type inference function makes use of these features to do its null handling 
compactly instead of writing an if statement

```kotlin
override fun visitReturnStatement(node: ReturnStatement): Any {
    return node.expression?.visit(this) ?: Type.Unit
}
```

Lastly we take a look at the `SemanticChecker` class. Semantic analysis statically verifies conditions that we cannot
catch with our grammar, such as making sure that variables are declared before they're used, that the types of
expressions always match the expected types, and that functions calls provide the correct number of arguments to the
functions. All of this is done in scopes. A Scope is defined as a map containing identifiers and types, and possibly a
reference to the enclosing scope as well. When referencing a variable, we start in the tightest scope and work our way
up, looking for the tightest match. This allows for variable shadowing in scopes.

We present the semantic verification of an if statement as an example.

```kotlin
override fun visitIfStatement(node: IfStatement) {
    if (node.condition.visit(inferenceVisitor) != Type.Boolean) {
        throw TypeException("Expected ${Type.Boolean} in if condition.")
    }

    node.condition.visit(this)

    node.trueStatement.visit(this)
    node.falseStatement?.visit(this)
}
```

As discussed before the type inference is lazy and the visitor has to visit the condition first to determine its type.
We then make sure that it is a boolean and throw an exception otherwise. To make sure that all of the parts of the if
statement are verified, we visit them all with our visitor. The condition is visited to make sure that all the used
variables have been declared. Once again, Kotlin's null safety spares us from writing an if. If our statement has no
else statement associated to it, Kotlin will simply ignore the `visit` call to it.

#### AST Interpreter

The interpreter is located in the `interpreter` package. It is essentially yet another visitor, evaluating the nodes of
our tree one by one. Tree interpreters are very easy to implement, but come at a high performance cost. Any commonly
used programming language will be faster than our simple tree interpreter in just about any benchmark. We start
interpreting at our start production, Program, which is also the root of our tree, simply telling our visitor to visit
all of its statements.

```kotlin
override fun visitProgram(node: Program) {
    node.statements.forEach { it.visit(this) }
}
```

Visiting a block is similar, but it leads us into a new scope and reverts back to the parent scope after all of the
statements have been visited.

```kotlin
override fun visitBlock(node: Block) {
    currentScope = Scope(currentScope)
    node.statements.forEach { it.visit(this) }
    currentScope = currentScope.parent!!
}
```

For variable declarations, we simply want to insert the value into our current scope. We use the `setDirect` method to
add the value directly into the scope itself. Without it, the interpreter would first check if there's a variable with
the same name in any of the ancestor scopes, and then overwrite it if found, which would lead to incorrect behavior.

Notice that we don't need to check if the value is already in the scope, since the semantic analysis is done before the
interpretation. 
```kotlin
override fun visitInitialization(node: Initialization) {
    currentScope.setDirect(node.id.value, node.value.visit(this))
}
```

We take a look at the `if` statement to see a more complicated construct. `expect` is a function defined in the
Interpreter class, which is effectively just a cast that throws a compiler exception if it fails. A failing cast in the
interpreting stage means that the semantic analysis made a mistake at some point and allowed an incorrect program to
continue executing.

The logic behind the interpreting step is pleasingly simple. We interpret the condition, check if it's true and execute
the `if` branch if so, otherwise we execute the `else` branch if one is present.

```kotlin 
override fun visitIfStatement(node: IfStatement) {
    if (expect(node.condition.visit(this))) {
        node.trueStatement.visit(this)
    } else {
        node.falseStatement?.visit(this)
    }
}
```

Lastly, we will mention a dirty hack we use for return values. We must stop interpreting when we hit a `return`
statement, but there's no easy way to "escape" out of a visitor. To go around this problem, we throw a custom exception
every time we see a `return` statement. The function call interpreter function catches this exception, unwraps the
return value and uses it. We took this idea from the wonderful series
[craftinginterpreters](http://craftinginterpreters.com/), which we can only recommend for learning the details of how
to write a functioning interpreter.

```kotlin
override fun visitReturnStatement(node: ReturnStatement): Any {
    throw Return(node.expression?.visit(this))
}
```

### Future work

Given that this is a university project, it was not given nearly as much love as it should have been. Due to a lack of
time, the language has been kept relatively simple and the design suffered a bit as well. We intend to continue working
on the project and list some interesting topics in this section.

Firstly, the feature set of the language could do with an expansion. A major feature that the language is missing are
arrays or any notion of structs. Implementing these would certainly allow for more interesting benchmarks and
comparisons, and is therefore a priority when thinking about moving forward.

Other features that would be interesting to have are more data types (primarily integers and strings), for loops and a
proper type hierarchy. A proper type hierarchy is especially interesting when it comes to functions. The `println` 
function only works with floats in the current implementation, although it could theoretically support all other types
as well. Currently, the only workaround would be to provide specialized function for every possible data type, which is
certainly not optimal. All of these features have been left out of the initial implementation to keep the complexity
relatively low.

On the technical side, we would like to see more than the two backends that we currently offer. A register-based
interpreter would be interesting for comparing performance, and so would an LLVM backend, with a possible JIT. 