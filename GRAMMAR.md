# Clam Language Grammar

This document specifies the formal grammar for the language using the EBNF syntax.
The same grammar is used to implement the parser and referenced in the documentation of
the parser's methods.

## Structure overview

The grammar specification is divided into four main parts, listed below.

* **Basic symbol definitions** - The definitions of the basic symbols used to
  define all the others. Here we define what we consider a letter or a number, for example.
  
* **Building blocks** - The most basic and simplest parts of the language. Includes the definition
  for identifiers, literals and operators.
  
* **Expressions** - The definitions of the language's expressions. To allow for a natural definition
  of operator precedence, the expression definition has been split up into three subproductions. They
  are formally specified in this section.
  
* **Statements** - All of the statements the language contains, such as assignments, `if` statements or
  similar, are listed in this section. The start production, `program` is also included in this section.


## Grammar definition

```
(* Basic symbol definitions *)
alpha = ? Anything considered a letter by the Unicode standard ? ;
num   = "0" | "1" | "2" | ... | "9" ;
ident_char = alpha | "_" ;
num_1 = num , { num } ;
```

```
(* Building blocks *)
identifier       = alpha , { ident_char } ;
float_constant   = num_1 , [ "." , num_1 ] ;
boolean_constant = "true" | "false" ;
type_name        = "float" | "bool" | "unit" ;

assignment_operator = "=" | "+=" | "-=" | "*=" | "/=" ;

function_call  = identifier , "(" , [ param_bindings ] , ")"
param_bindings = expression , { "," expression }

function_declaration = "fn" , identifier , "(" args , ")", [ ":" , return_type] , block ;
return_type          = ":" , type_name
args                 = arg , { "," , arg }
arg                  = identifier , ":" , type_name
```

```
(* Expressions *)
expression = expr3 ;
expr3 = expr2 , [ ( "==" | "<" | ">" | "<=" | ">=" ) , expr2 ]
expr2 = expr1 , { ( "+" | "-" ) , expr1 } ;
expr1 = expr0 , { ( "*" | "/" ) , expr2 } ;
expr0 = "(" , expression , ")"
      | ( "+" | "-" ) , expr0
      | identifier
      | float_constant
      | boolean_constant
      | function_call
      ;
```

```
(* Statements *)
program = { program_part } ;  (* Start production *)

program_part = statement
             | function_declaration
             ;

block = "{" , { statement } , "}" ;

statement = initialization_statement
          | assignment_statement
          | block
          | if_statement
          | while_statement
          | expression , ";"
          | "return" , [expression]
          ;

if_statement = "if" , expression , statement , [ "else" , statement ] ;

while_statement = "while" , expression , statement

initialization_statement = "let" , identifier , "=" , expression , ";" ;
assignment_statement = identifier, assignment_operator, expression ;
```